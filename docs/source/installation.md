# Installation

{{ project }} requires Python 3.7 or later.

```{code-block} shell
pip install pypergraph
```