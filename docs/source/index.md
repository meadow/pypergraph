% pypergraph documentation master file, created by
% sphinx-quickstart on Wed Jan 26 22:43:44 2022.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# {{ project }}

```{toctree}
:hidden:

tutorial.md
installation.md
reference/index.md

```

{{ project }} is a Python package that provides a hypergraph data structure.

If you are new to {{ project }}, see the [installation instructions][install]
and [tutorial].

[install]: installation.md
[tutorial]: tutorial.md